def read_txt_file(filename):
    """
    Reads a text file and returns a list of lines.
    """
    with open(filename, 'r', encoding='UTF-8') as f:
        return f.readlines()


gifts = read_txt_file('lw.txt')
# print(gifts)
result = []
ids = []
logs = []
for log in gifts:
    contents = log.split()
    entry = {}
    # print(contents)
    id = contents[2]
    time = contents[0]+' '+contents[1]
    gift = contents[4]
    entry = {'id': id, 'time': time, 'gift': gift}
    logs.append(entry)
    if id not in ids:
        ids.append(id)

# print(ids)
# print(logs[0])
for id in ids:
    all_gifts_with_time = []
    for log in logs:
        if log['id'] == id:
            all_gifts_with_time.append(
                {'time': log['time'], 'gift': log['gift']})
    print(id, all_gifts_with_time)
    gifts = []
    for entry in all_gifts_with_time:
        gifts.append(entry['gift'])
    gifts = set(gifts)
    print(id, gifts)
    for gift in gifts:
        counter = 0
        for entry in all_gifts_with_time:
            if entry['gift'] == gift:
                counter += 1
        print(id, gift, counter)
